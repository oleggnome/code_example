import axios from 'axios'

export default {
  state: {
    user: {
      last_name: null,
      calendar_view: null,
      first_name: null,
      email: null,
      start_page: null,
      phone: null,
      token: null
    }
  },
  mutations: {
    updateUser (state, payload) {
      // state.user = payload
      state.user.last_name = payload.last_name
      state.user.calendar_view = payload.settings.calendar_view
      state.user.first_name = payload.first_name
      state.user.email = payload.email
      state.user.start_page = payload.settings.start_page
      state.user.phone = payload.phone
      state.user.token = payload.token
    },
    updateToken (state, payload) {
      state.user.token = payload.token
    }
  },
  actions: {
    updateUser ({commit}, payload) {
      commit('updateUser', payload)
    },
    updateToken ({commit}, payload) {
      commit('updateToken', payload)
    }
  },
  getters: {
    getUser (state) {
      return state.user
    },
    async checkUser (state) {
      axios.defaults.headers.common['token'] = state.user.token
      await axios.post(window.backhost + 'profile/', {
        body: JSON.stringify({}),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'token': state.user.token
        }
      }).then(jsonData => {
        if (jsonData.data.res === 0) {
          return false
        } else {
          console.log(jsonData.data.data)
          // this.dispatch('updateUser', jsonData.data.data)
          return jsonData.data.data
        }
      }).catch(e => {
        return false
      })
    },
    getDefaultPage (state) {
      return state.user.start_page
    }
  }
}
