from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers

from authapp import models
from authapp.models import Token, User

from base import consts


class TokenCreateSerializer(serializers.Serializer):
    password = serializers.CharField(required=False, style={"input_type": "password"})

    default_error_messages = {
        "invalid_credentials": consts.INVALID_CRED,
        "inactive_account": consts.INACTIVE_ACCOUNT_ERROR,
        "invalid_role": consts.INVALID_ROLE
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self.fields['email'] = serializers.CharField(required=False)

    def validate(self, attrs):
        password = attrs.get("password")
        params = {'username': attrs.get('email')}
        self.user = authenticate(**params, password=password)
        if not self.user:
            self.fail("invalid_credentials")
        if not (self.user.email_verified or self.user.phone_verified):
            self.fail('inactive_account')

        return attrs


class UserSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    settings = serializers.SerializerMethodField('get_settings')

    class Meta:
        model = models.User
        fields = ('id', 'phone', 'email', 'first_name', 'last_name', 'middle_name', 'avatar', 'settings', )

    def get_settings(self, obj):
        return obj.get_view_settings()


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    token = serializers.SerializerMethodField(source='get_token')

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'phone', 'password', 'token')

    def get_token(self, user):
        return Token.objects.filter(user=user).first().key

    def validate_email(self, email):
        email = email.lower().strip()

        self.user = User.objects.filter(email=email, email_verified=True).first()
        if self.user and self.user.email_verified:
            raise serializers.ValidationError('user with this email already exists')

        return email

    def validate_phone(self, phone):
        self.user = User.objects.filter(phone=phone, phone_verified=True).first()
        if self.user and self.user.phone_verified:
            raise serializers.ValidationError('user with this phone already exists')

        return phone

    def create(self, validated_data):
        if self.user:
            return self.user
        return super().create(validated_data)
