from rest_framework.authentication import TokenAuthentication as DefaultTokenAuthentication

from authapp.models import Token


class TokenAuthentication(DefaultTokenAuthentication):
    model = Token
