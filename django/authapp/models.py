import json

from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

from base import utils
from pografiku import settings


class User(AbstractUser, PermissionsMixin):
    phone = models.BigIntegerField(unique=True, verbose_name='Телефон', null=True, blank=True)
    email = models.EmailField(unique=True, verbose_name='E-mail', null=True, blank=True)

    first_name = models.CharField(max_length=255, verbose_name='Имя', null=True, blank=True)
    middle_name = models.CharField(max_length=255, verbose_name='Отчество', null=True, blank=True)
    last_name = models.CharField(max_length=255, verbose_name='Фамилия', null=True, blank=True)

    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    avatar = models.ImageField(upload_to='users/', blank=True)

    phone_verified = models.BooleanField(default=False)
    email_verified = models.BooleanField(default=False)

    profession = models.CharField(max_length=255, blank=True, verbose_name='Профессия')
    created_at = models.DateTimeField(auto_now_add=True)

    default_settings = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['-created_at']

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.first_name + ' ' + self.middle_name + ' ' + self.last_name

    def get_short_name(self):
        short_name = self.last_name
        if self.first_name:
            short_name += ' ' + self.first_name[0] + '.'
        if self.middle_name:
            short_name += ' ' + self.middle_name[0] + '.'
        return short_name

    def update_view_settings(self, start_page='calendar', calendar_view='month'):
        self.default_settings = json.dumps({
            'start_page': start_page,
            'calendar_view': calendar_view
        })
        self.save()

    def get_view_settings(self):
        if self.default_settings:
            return json.loads(self.default_settings)
        else:
            self.update_view_settings()
            return {
                        'start_page': 'calendar',
                        'calendar_view': 'month'
                    }


class Token(models.Model):
    key = models.CharField(_("Key"), max_length=40, primary_key=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='auth_tokens',
        on_delete=models.CASCADE, verbose_name=_("User")
    )
    type = models.CharField(max_length=30, blank=True, db_index=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        verbose_name = 'токен'
        verbose_name_plural = 'REST-Токены'

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = utils.random_string(32)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.key
