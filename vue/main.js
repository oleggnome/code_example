import Vue from 'vue'
import VueRouter from 'vue-router'
import vClickOutside from 'v-click-outside'

// Plugins
import GlobalComponents from './gloablComponents'
import Notifications from './components/UIComponents/NotificationPlugin'
import SideBar from './components/UIComponents/SidebarPlugin'
import App from './App'

// router setup
import routes from './routes/routes'

// library imports
import Chartist from 'chartist'
import 'bootstrap/dist/css/bootstrap.css'
import './assets/sass/paper-dashboard.scss'
import 'es6-promise/auto'
import vmodal from 'vue-js-modal'
import fullCalendar from 'vue-fullcalendar'

import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'

import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import axios from 'axios'

import VueProgressBar from 'vue-progressbar'

import VTooltip from 'v-tooltip'

library.add(faCoffee)

// plugin setup
Vue.use(VueRouter)
Vue.use(GlobalComponents)
Vue.use(vClickOutside)
Vue.use(Notifications)
Vue.use(SideBar)
Vue.use(vmodal, { dialog: true })
Vue.component('full-calendar', fullCalendar)
Vue.use(Loading)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(require('vue-moment'))
Vue.use(VueProgressBar, {
  color: 'rgb(255, 161, 161)',
  failedColor: 'red',
  thickness: '5px'
})
Vue.use(VTooltip)
var subConfig
if (process.env.NODE_ENV === 'development') {
  subConfig = require('../config/dev.env.js')
} else {
  subConfig = require('../config/prod.env.js')
}
const router = new VueRouter({
  routes,
  linkActiveClass: 'active'
})

Object.defineProperty(Vue.prototype, '$Chartist', {
  get () {
    return this.$root.Chartist
  }
})
window.backhost = subConfig.API_URL

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
  data: {
    Chartist: Chartist
  },
  created () {
    axios.defaults.headers['Authorization'] = 'Token ' + localStorage.token
    axios.get(window.backhost + 'api/profile/').then(jsonData => {
      if (jsonData.status === 200) {
        this.$store.dispatch('updateUser', jsonData.data.results[0])
        this.progressBar = false
      } else {
        this.$router.push('/login')
      }
    }).catch(e => {
      this.$router.push('/login')
    })
  }
})
