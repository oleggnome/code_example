from django.views.generic import DetailView
from rest_framework import generics, status, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from authapp.models import Token, User
from authapp.serializers import TokenCreateSerializer, UserSerializer, UserRegistrationSerializer

from authapp import models as models_auth


class LoginView(generics.GenericAPIView):
    serializer_class = TokenCreateSerializer
    permission_classes = (AllowAny, )

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        token = Token.objects.create(user=serializer.user)
        if serializer.user:
            user_serializer = UserSerializer(instance=serializer.user)

        return Response(
            data={'token': token.key, 'user': user_serializer.data}, status=status.HTTP_200_OK
        )


class RegistrationView(generics.CreateAPIView):
    model = User
    serializer_class = UserRegistrationSerializer
    permission_classes = (AllowAny, )

    def perform_create(self, serializer):
        user = serializer.save()
        user.set_password(serializer.validated_data['password'])
        user.save()
        Token.objects.create(user=user)
        # if user.email:
        #     VerificationCode.create_for_email(user)
        # if user.phone:
        #     VerificationCode.create_for_sms(user)
        return user
